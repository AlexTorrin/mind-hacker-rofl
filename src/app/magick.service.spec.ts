import { TestBed, inject } from '@angular/core/testing';

import { MagickService } from './magick.service';

describe('MagickService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MagickService]
    });
  });

  it('should be created', inject([MagickService], (service: MagickService) => {
    expect(service).toBeTruthy();
  }));

  it('possibleApeals must be more then one', inject([MagickService], (service: MagickService) => {
     expect(service.possibleAppeals.length > 1).toBe(true);
  }));

  it('steps must be more then three', inject([MagickService], (service: MagickService) => {
     expect(service.steps > 3).toBe(true);
  }));

  it('apeals must be more then three', inject([MagickService], (service: MagickService) => {
     expect(service.appeals.length > 3).toBe(true);
  }));

  it('currentApeal must exist', inject([MagickService], (service: MagickService) => {
     expect(service.currentApeal).toBeTruthy();
  }));

  it('next step should not make everythin in ass',  inject([MagickService], (service: MagickService) => {
      while(service.next()) { }
      expect(service.currentApeal.value).toBeDefined();
  }));

});
