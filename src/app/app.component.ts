import { MagickService, Appeal } from './magick.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [MagickService]
})
export class AppComponent {

    isInitial = true;
    currentAppeal: Appeal;

    constructor(private magick: MagickService) {
        this.magick.currentApeal.subscribe(
          appeal => this.currentAppeal = appeal
        );
    }

    next() {
    }


}
