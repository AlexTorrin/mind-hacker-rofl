import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class MagickService {

    possibleAppeals: Array<Appeal> = [];
    appeals: Array<Appeal> = [];
    currentApeal: BehaviorSubject<Appeal> = new BehaviorSubject<Appeal>(null);

    steps = Math.round(Math.random() * 7) + 3;
    step = 0;
    answer = 0;

    constructor() {
        this.initPossibleApeals();
        this.initAppeals();
        this.calculateAnswer();
    }

    initPossibleApeals() {
        this.possibleAppeals.push( new Appeal('Прибавь ', (x, y) => x + y ) );
        this.possibleAppeals.push( new Appeal('Отними ', (x, y) => x - y ) );
    }

    initAppeals() {
        let step = 0;
        while (step <= this.steps) {
            const newApeal = this.possibleAppeals[ Math.round(Math.random() * (this.possibleAppeals.length - 1) ) ];
            newApeal.number = Math.round(Math.random() * 7) + 3;
            this.appeals.push(newApeal);
            step++;
        }
        this.currentApeal.next(this.appeals[this.step]);
    }

    calculateAnswer() {
        this.appeals.forEach( currAppeal => this.answer = currAppeal.actionAppeal(this.answer, currAppeal.number));
    }

    next() {
        if ( this.step >= this.appeals.length ) {
            return false;
        }

        this.currentApeal.next(this.appeals[this.step]);
        this.step++;

        return true;
    }

}

export class Appeal {

    textAppeal: string;
    actionAppeal: (x, y) => number;
    number: number;

    constructor(textAppeal, actionAppeal) {
        this.textAppeal = textAppeal;
        this.actionAppeal = actionAppeal;
    }

}
