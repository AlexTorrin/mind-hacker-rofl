import { MindHackerRoflPage } from './app.po';

describe('mind-hacker-rofl App', () => {
  let page: MindHackerRoflPage;

  beforeEach(() => {
    page = new MindHackerRoflPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
